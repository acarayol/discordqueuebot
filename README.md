# DiscordQueueBot

A Discord bot for managing a waiting queue in every text channel of a server. 
All members of the server can join/leave the queue. Members with the @Prof role can pop members from the queue.

## Features
- Queues can opened and closed by @Prof members
- The bot tries to keep the disruption too a minimum:
	- by cleaning commands from members
	- by displaying the queue status only once in a channel

## Limitations
- Currently the content of the queues is lost between restart of the bot.
- The bot can only handle one server
- The messages are in french (but this could be easily changed by editing the sources)

## Installation

1. This bot is written in python and uses [discord.py](https://github.com/Rapptz/discord.py) Python wrapper for the Discord API. It requires Python 3.5.3 or latter
and discord.py 1.3.3 or greater.

Install the latest version of `discord.py` with:
```bash
python3 -m pip install -U discord.py
```

2. You need replace the variable `DISCORD_TOKEN` the bot token you obtained from Discord. You can also put your token in the `DISCORD_QUEUE_BOT_TOKEN` environment variable (which takes precedence ovre the variable `DISCORD_TOKEN`).

## Usage

Run the bot with:
```bash
python3 discord-queue-bot.py
``` 

Commands are prefixed by ! and can only be used in text channels.

All members can:
- get help with `!help`,
- join or leave the queue of a text channel with `!join` and `!leave`,
- print the queue for the text channel with `!status`. 

Member with the @prof role can:
- open or close the queue of a text channel with `!open`and `!close`,
- pop members from the queue with `!pop`. The member is notified in the channel and via private message (PM)


