import discord
from discord.ext import commands
import datetime

TIMEOUT_TEMPORARY_MESSAGE_IN_SECONDS = 10
PROF_ROLE = "@prof"
DISCORD_TOKEN = "ADD YOUR DISCORD TOKEN HERE"

HELP_MSG_STUDENT = """!status -- affiche la file d'attente du salon
!join -- prendre place dans la file d'attente du salon
!leave -- quitter la file d'attente du salon
"""

HELP_MSG_PROF = """!status -- état de la file d'attente du salon
!join -- prendre place dans la file d'attente du salon
!leave -- quitter la file d'attente du salon
!pop -- appel le premier étudiant de la file du salon
!clear -- vide la file du salon
!open -- ouvre la file du salon
!close -- ferme la file du salon
"""

QUEUE_EMBED_TITLE = "File d'attente"


def current_time():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


class Queue:

    def __init__(self):
        self.queue = []

    def enqueue(self, member):
        if member not in self.queue:
            self.queue.append(member)
            return True
        else:
            return False

    def remove(self, member):
        if member in self.queue:
            self.queue.remove(member)
            return True
        else:
            return False

    def pop(self):
        if len(self.queue) < 1:
            return None
        return self.queue.pop(0)

    def size(self):
        return len(self.queue)

    def __str__(self):
        if len(self.queue) == 0:
            return "La file est vide. C'est le moment d'en profiter!"
        res = [str(i + 1) + ". " + member.display_name for (i, member) in enumerate(self.queue)]
        return "\n".join(res)

    def clear(self):
        self.queue.clear()


class ChannelQueue():
    def __init__(self, channel):
        self.queue = Queue()
        self.opened = True
        self.channel = channel
        self.display_msg = None  # Message showing the content of the queue

    async def open(self):
        if self.opened:
            return
        self.opened = True
        await self.update_display()

    async def close(self):
        if not self.opened:
            return
        self.opened = False
        self.queue.clear()
        await self.send_temporary_msg("La file du salon " + self.channel.mention + " est fermée.")
        await self.update_display()

    async def check_opened(self):
        if not self.opened:
            await self.send_temporary_msg("La file du salon " + self.channel.mention + " est fermée.")
        return self.opened

    async def add_member(self, member):
        assert isinstance(member, discord.Member)
        if not await self.check_opened():
            return
        if self.queue.enqueue(member):
            await self.send_temporary_msg(member.mention + " a rejoint la file du salon " + self.channel.mention)
            await self.update_display()

    async def clear_display(self):
        if self.display_msg is None:
            return
        try:
           await self.display_msg.delete()
        except KeyboardInterrupt:
            raise
        except:
            pass
        finally:
            self.display_msg = None

    async def update_display(self):
        await self.clear_display()
        if self.opened:
            queue_content=str(self.queue)
        else:
            queue_content="Cette file est fermée."
        embed = discord.Embed(title="File d'attente du salon #" + self.channel.name, description=queue_content)
        self.display_msg = await self.channel.send("", embed=embed)

    async def remove_member(self, member):
        assert isinstance(member, discord.Member)
        if not await self.check_opened():
            return
        if self.queue.remove(member):
            await self.send_temporary_msg(member.mention + " a quitté la file du salon " + self.channel.mention)
            await self.update_display()

    async def pop(self):
        if not await self.check_opened():
            return
        member = self.queue.pop()
        if member is None:
            await self.send_temporary_msg("La file du salon " + self.channel.mention + " est vide.")
            return
        await member.send("C'est votre tour!")
        await self.send_temporary_msg("C'est le tour de " + member.mention, fade_timeout=None)
        await self.update_display()

    async def send_temporary_msg(self, msg, fade_timeout=TIMEOUT_TEMPORARY_MESSAGE_IN_SECONDS):
        await send_temporary_msg(msg, self.channel, fade_timeout=fade_timeout)


async def send_temporary_msg(msg, channel, fade_timeout=TIMEOUT_TEMPORARY_MESSAGE_IN_SECONDS):
    embed = discord.Embed(title="", description=msg)
    await channel.send("", embed=embed, delete_after=fade_timeout)


class QueueBot(commands.Bot):

    def __init__(self, *args, **kwargs):
        super().__init__(command_prefix="!")
        self.queue = Queue()
        self.command_prefix = "!"
        self.list_prints = []
        self.guild = None  # The guild (or server) of the bot is initialized in on_ready

    def get_member(self, user):
        if isinstance(user, discord.Member):
            return user
        if not isinstance(user, discord.User):
            return None
        return self.guild.get_member(user.id)

    @staticmethod
    def is_prof(member):
        assert isinstance(member, discord.Member)
        for role in member.roles:
            if role.name == PROF_ROLE:
                return True
        return False

    async def parse_context(self, ctx):
        ok = True
        author = self.get_member(ctx.author)
        if author is None:
            ok = False
        queue_channel = None
        if ctx.channel not in self.text_channels:
            await send_temporary_msg("Les commandes ne peuvent être utilisées que dans les salons.", ctx.channel)
            ok = False
        else:
            queue_channel = self.text_channels[ctx.channel]
        return author, queue_channel, ok

    async def on_ready(self):
        print(current_time() + ' The QueueBot 2000 is online.')
        if len(client.guilds) != 1:
            print("The list of servers should contain only one server : " + str(client.guilds))
            exit(0)
        self.guild = client.guilds[0]
        print(current_time() + ' Connected to ' + str(client.guild))
        self.text_channels = {}
        print(current_time() + " Providing queue for the textChannels:")
        for channel in self.guild.text_channels:
            print("\t" + channel.name)
            self.text_channels[channel] = ChannelQueue(channel)
        # await client.change_presence(activity=discord.CustomActivity(name="Type !help for some ... help"))


client = QueueBot()
client.remove_command("help")


async def clean(ctx):
    try:
        if ctx.channel in client.text_channels:
            await ctx.message.delete()
    except KeyboardInterrupt:
        raise
    except:
        pass

def command(f):
    async def wrapper(ctx):
        try:
            print(current_time() + " Command " + str(ctx.message.content) + " from " + str(
                ctx.author) + " on channel " + str(ctx.channel))
            author, queue_channel, ok = await client.parse_context(ctx)
            if not ok:
                return
            await f(author, queue_channel)
        finally:
            await clean(ctx)

    return wrapper


def only_prof(f):
    async def wrapper(author, queue_channel):
        if not client.is_prof(author):
            await author.send("Nice try!")
            return
        await f(author, queue_channel)

    return wrapper


@client.command(name="help")
@command
async def help(author, queue_channel):
    if client.is_prof(author):
        await queue_channel.send_temporary_msg(HELP_MSG_PROF)
    else:
        await queue_channel.send_temporary_msg(HELP_MSG_STUDENT)


@client.command(name="join")
@command
async def join(author, queue_channel):
    await queue_channel.add_member(author)


@client.command(name="leave")
@command
async def leave(author, queue_channel):
    await queue_channel.remove_member(author)


@client.command(name="status")
@command
async def status(author, queue_channel):
    await queue_channel.update_display()


@client.command(name="pop")
@command
@only_prof
async def pop(author, queue_channel):
    await queue_channel.pop()


@client.command(name="open")
@command
@only_prof
async def open(author, queue_channel):
    await queue_channel.open()


@client.command(name="close")
@command
@only_prof
async def close(author, queue_channel):
    await queue_channel.close()


client.run(DISCORD_TOKEN)
